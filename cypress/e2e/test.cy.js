describe('template spec', () => {

  it('retour de trajet', () => {
    cy.visit('https://front-five-umber.vercel.app/')
    cy.get('#departure').type("Paris");
    cy.get('#arrival').type("Marseille");
    cy.get('#date').type("2024-02-06");
    cy.get('#search').click();
    cy.contains("€");

  })
  it('booking', () => {
    cy.visit('https://front-five-umber.vercel.app/')
    cy.get('#departure').type("Paris");
    cy.get('#arrival').type("Marseille");
    cy.get('#date').type("2024-02-06");
    cy.get('#search').click();
    cy.get("button[id='65c35c7c0a7ace64541f67f3']").click()
    cy.contains("Paris > Marseille")
  })
  
})